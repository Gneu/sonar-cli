# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.0] - 2018-07-26

### Added

-   Generation now includes `--ci` flag to load `BRANCH_NAME` from environment
-   Interactive mode on init

### Fixed

-   Corrected issue with new lines causing linux machines to fail to execute application directly.

## [1.1.0] - 2018-07-24

### Changed

-   Now we ensure we have defined values before adding the key to our listing.

### Fixed

-   Correctly replacing invalid characters with a `-`.

## [1.0.2] - 2018-07-18

### Changed

-   corrected issue with the naming of the file, resulting in a temporary file being created.

### Removed

-   Removed temporary file - `_sonar-project.properties`

## [1.0.1] - 2018-07-18

### Added

-   `generated` as a command, converting from the `package.sonar` object to your sonar configuration.
-   `init` as a command, to create your `package.sonar` object.

[1.2.0]: https://gitlab.com/Gneu/sonar-cli/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/Gneu/sonar-cli/compare/1.0.2...1.1.0
[1.0.2]: https://gitlab.com/Gneu/sonar-cli/compare/1.0.1...1.0.2
[1.0.1]: https://gitlab.com/Gneu/sonar-cli/compare/7957b69f...1.0.1
[unreleased]: https://gitlab.com/Gneu/sonar-cli/compare/master...develop
