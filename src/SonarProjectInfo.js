'use strict';

let branch = require('git-branch');

module.exports = {
	SonarProjectInfo: function(projectInfo) {
		projectInfo = projectInfo || {};

		this.projectKey = projectInfo.projectKey || '';
		this.branch = projectInfo.branch || {};

		this.organization = projectInfo.organization || '';
		this.sources = projectInfo.sources || ['./src'];
		this.tests = projectInfo.tests || ['./tests'];

		this.sourceEncoding = projectInfo.sourceEncoding || 'UTF-8';

		this.host = projectInfo.host || { url: '' };

		this.links = projectInfo.links || {
			issue: '',
			scm: '',
			ci: ''
		};

		this.testExecutionReportPaths = projectInfo.testExecutionReportPaths || '';

		this.other = projectInfo.other || {};

		// TODO: Handle other configuration values here
	}
};

module.exports.SonarProjectInfo.prototype = Object.assign(
	{
		toPropertiesListing: function(ci, pkg) {
			let branchName = branch.sync() || '';
			pkg = pkg || {};

			if (ci) {
				branchName = process.env.BRANCH_NAME;
			}

			branchName = branchName.replace(/[^a-z0-9-_.:]/gi, '-');
			this.projectVersion = pkg.version || 'not provided';

			if (this.projectKey.indexOf('{{branchName}}') >= 0) {
				this.projectKey = this.projectKey.replace('{{branchName}}', branchName);
			} else if (this.branch.name && this.branch.name.indexOf('{{branchName}}') >= 0) {
				this.branch.name = this.branch.name.replace('{{branchName}}', branchName);
			}

			if (this.projectKey.indexOf('{{projectName}}') >= 0) {
				this.projectKey = this.projectKey.replace('{{projectName}}', pkg.name);
			}

			this.projectName = pkg.name;
			this.projectDescription = pkg.description;

			// TODO: Implement full git-flow configurations here
			if (!this.leak) {
				if (branchName === 'develop') {
					this.leak = {
						period: 'master'
					};
				} else {
					this.leak = {
						period: 'develop'
					};
				}
			}

			let results = Object.keys(this)
				.filter((key) => {
					return key !== 'other' && !!this[key];
				})
				.map((key) => {
					let value = this[key];

					if (Array.isArray(value)) {
						if (this[key].length > 0) {
							return `${key}=${this[key].join(',')}`;
						}
					} else if ((typeof value === 'object' && value !== null) || typeof value === 'function') {
						let params = Object.keys(this[key]);
						let results = [];

						params.forEach((el) => {
							if (this[key][el]) {
								results.push(`${key}.${el}=${this[key][el]}`);
							}
						});

						return results;
					} else {
						if (this[key]) {
							return `${key}=${this[key]}`;
						}
					}
				});

			results.push(
				Object.keys(this['other']).map((key) => {
					let value = this['other'][key];

					if (Array.isArray(value)) {
						if (this['other'][key].length > 0) {
							return `${key}=${this['other'][key].join(',')}`;
						}
					} else if ((typeof value === 'object' && value !== null) || typeof value === 'function') {
						let params = Object.keys(this['other'][key]);
						let results = [];

						params.forEach((el) => {
							if (this['other'][key][el]) {
								results.push(`${key}.${el}=${this['other'][key][el]}`);
							}
						});

						return results;
					} else {
						if (this['other'][key]) {
							return `${key}=${this['other'][key]}`;
						}
					}
				})
			);

			return results.reduce((acc, val) => acc.concat(val), []).map((line) => {
				return `sonar.${line}`;
			});
		}
	},
	Object.prototype
);
