'use strict';

const { resolve } = require('path');
const { writeFile } = require('fs');
const { prompt } = require('inquirer');

const { SonarProjectInfo } = require('./SonarProjectInfo');

const packageFileName = 'package.json';
const sonarProjectFileName = 'sonar-project.properties';

// TODO: Provide interactive mode
exports.IntitializePackage = (options) => {
	const destination = resolve(options.path);

	if (options.interactive) {
		prompt([
			{
				type: 'expand',
				message: `Did you mean to attempt to load this file: \n${destination}/${packageFileName}`,
				name: 'confirmation',
				choices: [
					{
						key: 'y',
						name: 'Yes',
						value: true
					},
					{
						key: 'n',
						name: 'No',
						value: false
					}
				]
			}
		]).then((answers) => {
			if (!answers.confirmation) {
				console.log('Halting initialization.');
				return;
			}

			console.log('Loading package.');
			const pkg = require(resolve(destination, packageFileName));

			if (pkg.sonar) {
				prompt([
					{
						type: 'expand',
						message: `This file appears to have a sonar section. Would you ilke to overwrite it: \n ${JSON.stringify(
							pkg.sonar,
							null,
							'\t'
						)}`,
						name: 'overwrite',
						choices: [
							{
								key: 'y',
								name: 'Yes',
								value: true
							},
							{
								key: 'n',
								name: 'No',
								value: false
							}
						]
					}
				]).then((answers) => {
					if (answers.overwrite) {
						initializeAndSave(pkg, options.path);
					}
				});
			} else {
				initializeAndSave(pkg, options.path);
			}
		});
	} else {
		console.log('Loading package.');
		const pkg = require(resolve(destination, packageFileName));

		initializeAndSave(pkg, options.path);
	}
};

function initializeAndSave(pkg, path) {
	pkg.sonar = new SonarProjectInfo(pkg.sonar, pkg);

	writeFile(resolve(path, packageFileName), JSON.stringify(pkg, null, '\t'), (err) => {
		if (err) {
			console.error(err);
		} else {
			console.log('Completed. Please consult the configuration in your package.json file');
		}
	});
}

exports.GenerateSonarFile = (options) => {
	const destination = resolve(options.path);

	const pkg = require(resolve(destination, packageFileName));

	if (!pkg.sonar) {
		throw new Error('Tried to generate a sonar file without an initialized workspace.');
	}

	const sonar = new SonarProjectInfo(pkg.sonar, pkg);

	writeFile(
		resolve(options.path, sonarProjectFileName),
		sonar.toPropertiesListing(options.ci, pkg).join('\n'),
		(err) => {
			if (err) {
				console.error(err);
			} else {
				console.log('Completed. Please consult the configuration in your package.json file');
			}
		}
	);
};
