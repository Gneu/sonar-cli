#!/usr/bin/env node
'use strict';

const { Command } = require('commander');
const { IntitializePackage } = require('../src/package');

let command = new Command('initialize');

command.arguments('[path]');
command.action(function(path) {
	this.path = path;
});

command.option('-i, --interactive', 'Run application with interactive mode');

command.path = './';
command.interactive = false;

if (require.main === module) {
	command.parse(process.argv);

	IntitializePackage(command);
} else {
	exports.sonarInit = command;
}
