#!/usr/bin/env node
'use strict';

const { Command } = require('commander');
const { GenerateSonarFile } = require('../src/package');

let command = new Command('generate');

command.arguments('[path]');
command.action(function(path) {
	this.path = path;
});

command.option('--ci', 'Load values from Continuous Integration Environment Variables');

command.path = './';
command.ci = false;

if (require.main === module) {
	command.parse(process.argv);

	GenerateSonarFile(command);
} else {
	exports.sonarGenerate = command;
}
