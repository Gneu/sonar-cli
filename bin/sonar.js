#!/usr/bin/env node
'use strict';

const { Command } = require('commander');
const pkg = require('../package.json');

let command = new Command('sonar');

command.version(pkg.version, '-v, --version');

command.command('init [path]', 'initialize a default sonar-project.properties file').alias('initialize');

command.command('validate [path]', 'validate the sonar-project.properties file').alias('valid');

command.command('run [path]', 'run the sonar-scanner').alias('r');

command
	.command('generate [path]', 'generate the sonar-project.properties file based on your configuration', {
		isDefault: true
	})
	.alias('gen');

if (require.main === module) {
	command.parse(process.argv);
} else {
	exports.sonar = command;
}
