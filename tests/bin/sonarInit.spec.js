'use strict';

describe('Bin > Sonar Init', () => {
	let { sonarInit } = require('../../bin/sonar-init');

	it('should exist', () => {
		expect(sonarInit).not.toBeNull();
		expect(sonarInit).not.toBeUndefined();
	});

	describe('options', () => {
		it('should have at least one option', () => {
			expect(sonarInit.options.length).toEqual(1);
		});
	});

	describe('parsing arguments', () => {
		it('should default the path to `./`', () => {
			sonarInit.parse(['node', __filename]);

			expect(sonarInit.path).toEqual('./');
		});

		it('should parse third argument as the path', () => {
			sonarInit.parse(['node', __filename, __dirname]);

			expect(sonarInit.path).toEqual(__dirname);
		});
	});
});
