'use strict';

describe('Bin > Sonar', () => {
	let { sonar } = require('../../bin/sonar');

	it('should exist', () => {
		expect(sonar).not.toBeNull();
		expect(sonar).not.toBeUndefined();
	});

	describe('options', () => {
		it('should have at least one option', () => {
			expect(sonar.options.length).toEqual(1);
		});

		it('should have option -v', () => {
			let option = sonar.options[0];

			expect(option.short).toEqual('-v');
			expect(option.long).toEqual('--version');
		});
	});

	describe('commands', () => {
		let lookup = {};

		beforeAll(() => {
			sonar.commands.forEach((command) => {
				lookup[command._name] = command;
			});
		});

		it('should have 4 option', () => {
			expect(sonar.commands.length).toEqual(4);
		});

		it("should have at default command set to 'generate'", () => {
			expect(sonar.defaultExecutable).toEqual('generate');
		});

		it('should have a validate command', () => {
			expect(lookup['validate']).not.toBeUndefined();
			expect(lookup['validate']).not.toBeNull();
		});

		it('should have a run command', () => {
			expect(lookup['run']).not.toBeUndefined();
			expect(lookup['run']).not.toBeNull();
		});

		it('should have a init command', () => {
			expect(lookup['init']).not.toBeUndefined();
			expect(lookup['init']).not.toBeNull();
		});

		it('should have a generate command', () => {
			expect(lookup['generate']).not.toBeUndefined();
			expect(lookup['generate']).not.toBeNull();
		});
	});
});
