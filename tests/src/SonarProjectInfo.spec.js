'use strict';

function randomString() {
	return Math.random()
		.toString(8)
		.substr(2);
}

describe('Src > SonarProjectInfo', () => {
	const { SonarProjectInfo } = require('../../src/SonarProjectInfo');

	it('should exist', () => {
		expect(SonarProjectInfo).not.toBeNull();
		expect(SonarProjectInfo).not.toBeUndefined();
	});

	describe('default values', () => {
		let tmp;

		beforeEach(() => {
			tmp = new SonarProjectInfo();
		});

		it('should exist', () => {
			expect(tmp).not.toBeNull();
			expect(tmp).not.toBeUndefined();
		});

		['projectKey', 'organization', 'testExecutionReportPaths'].sort().forEach((key) => {
			it(`should have ${key} as a property`, () => {
				expect(tmp).toHaveProperty(key);
			});

			it(`${key} should be set to ""`, () => {
				expect(tmp[key]).toEqual('');
			});
		});

		it(`should have sourceEncoding as a property`, () => {
			expect(tmp).toHaveProperty('sourceEncoding');
		});

		it(`sourceEncoding should be set to 'UTF-8'`, () => {
			expect(tmp['sourceEncoding']).toEqual('UTF-8');
		});

		it(`should have sources as a property`, () => {
			expect(tmp).toHaveProperty('sources');
		});

		it(`sources should be set to ['./src']`, () => {
			expect(tmp['sources']).toEqual(['./src']);
		});

		it(`should have tests as a property`, () => {
			expect(tmp).toHaveProperty('tests');
		});

		it(`tests should be set to ['./tests']`, () => {
			expect(tmp['tests']).toEqual(['./tests']);
		});
	});

	describe('retaining values', () => {
		let tmp;
		let previousValue;

		beforeEach(() => {
			previousValue = new SonarProjectInfo();

			previousValue.projectKey = randomString();
			previousValue.organization = randomString();
			previousValue.sourceEncoding = randomString();
			previousValue.testExecutionReportPaths = randomString();
			previousValue.host.url = randomString();

			previousValue.links.scm = randomString();
			previousValue.links.ci = randomString();
			previousValue.links.issue = randomString();

			previousValue.sources = [randomString(), randomString(), randomString()];
			previousValue.tests = [randomString(), randomString(), randomString()];

			previousValue.other = {
				key: { value: 42 }
			};

			tmp = new SonarProjectInfo(previousValue);
		});

		it('should exist', () => {
			expect(tmp).not.toBeNull();
			expect(tmp).not.toBeUndefined();

			expect(previousValue).not.toBeNull();
			expect(previousValue).not.toBeUndefined();
		});

		[
			'projectKey',
			'organization',
			'host',
			'testExecutionReportPaths',
			'sourceEncoding',
			'sources',
			'tests',
			'links',
			'other'
		]
			.sort()
			.forEach((key) => {
				it(`should have ${key} as a property`, () => {
					expect(tmp).toHaveProperty(key);
				});

				it(`${key} should be set to the same value`, () => {
					expect(tmp[key]).toEqual(previousValue[key]);
				});
			});
	});
});
