# Sonar CLI

[![pipeline status](https://gitlab.com/Gneu/sonar-cli/badges/master/pipeline.svg)](https://gitlab.com/Gneu/sonar-cli/commits/master) [![coverage report](https://gitlab.com/Gneu/sonar-cli/badges/master/coverage.svg)](https://gitlab.com/Gneu/sonar-cli/commits/master) [![quality gate](https://sonarcloud.io/api/project_badges/measure?project=gneu-sonar-cli&metric=alert_status)](https://sonarcloud.io/dashboard?id=gneu-sonar-cli)

I hate having to have my sonar-project.properties file committed, considering how much of it is based entirely upon my project. This project intends to replace this dependency, by providing you with a generated file.

Enjoy.

## Setup & Configuration

1.  install this package globally
2.  run `sonar` or `sonar init` to initialize your project
3.  run `sonar generate` to generate the `sonar-project.properties` file for your current project
4.  run `sonar --version` or `sonar help` for more information about the application

### Configuration Parameters

https://docs.sonarqube.org/display/SONAR/Analysis+Parameters

## Contribution

I am always looking for fellow contributors. As this is an open source project
feel free to fork and help drive this application in a direction where it can be
considered more useful.

I appreciate all feedback, even the most minor.
